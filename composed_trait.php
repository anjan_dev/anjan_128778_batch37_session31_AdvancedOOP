<?php
trait hi{
    public function sayhi(){
        echo "hi ";
    }
}
trait hello{
    use hi;
    public function sayhello(){

        echo "hello ";
    }
}
class Myhelloworld {
    use hello;
    public function sayworld(){
        echo " world";
    }
}

$obj=new Myhelloworld();

$obj->sayhi();
$obj->sayhello();
$obj->sayworld();