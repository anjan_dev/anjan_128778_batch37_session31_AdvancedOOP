<?php
trait hi{
    public function sayhi(){
        echo "hi ";
    }
}
trait hiThere{
    public function sayhi(){
        echo "hi ";
    }}
trait hello{
    public function sayhi(){
        echo "hello ";
    }
}
class Myhelloworld {
    use hi,hello,hiThere{
        hello::sayhi insteadof hi,hithere;
        hi::sayhi as echohi;
    }
    public function sayworld(){
        echo " world";
    }
}

$obj=new Myhelloworld();

$obj->sayhi();
$obj->echohi();
$obj->sayworld();