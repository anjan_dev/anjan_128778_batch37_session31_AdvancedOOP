<?php
interface flyer{
    public function canFly();


}
interface swimmer{
    public function canSwim();
}
interface bird{
    public function canSwim();
    public function canFly();
}

class penguine implements swimmer{

    public function canSwim()
    {
        echo "i can swim";
    }
}
class eagle implements flyer{

    public function canFly()
    {
        echo "i can fly";
    }
}
class duck implements bird{

    public function canSwim()
    {
        echo "i can swim";
    }

    public function canFly()
    {
        echo " and i can fly";
    }
}
function describe($obj){

    if($obj instanceof bird)
    {
        $obj->canSwim();
        $obj->canFly();
    }
    else if ($obj instanceof flyer)
    {

        $obj->canFly();
    }
    else if($obj instanceof swimmer)
    {
        $obj->canSwim();
       
    }


}

describe(new duck());
